﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Catch_A_Critter
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        const int MAX_CRITTERS = 20;
        Critter[] critterPool = new Critter[MAX_CRITTERS];
 // a blank array for critters that will be made
        const float spawnDelay = 3f;
        float timeUntilNextSpawn = spawnDelay;
        int currentCritterIndex = 0;
        Button button = null;
        bool play = false;
        Timer Gametimer = null;
        const float gameLenght = 30f;


        Score ourScore; //empty varibable for the players score

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            ourScore = new Score();
            //call score load
            ourScore.LoadContent(Content);

            //initialise rng for critter species
            Random rand = new Random();

            //create critter objects and content
            for (int i = 0; i < MAX_CRITTERS; ++i)
            {
                Critter.Species newSpecies = (Critter.Species)rand.Next(0, (int)Critter.Species.NUM);
                //creating new critter and giving it score
                Critter newCritter = new Critter(ourScore,newSpecies );
                //loading content for new critter
                newCritter.LoadContent(Content);

                //add newly created critter to pool
                critterPool[i] = newCritter;
            }

            //create and load button
            button = new Button(); 
            button.LoadContent(Content);

            //set the function that will be called when the button is clicked
            //+= emans it aadds to any existing functions
            button.ourButtonCallBack += StartGame;

            //game timer being laoded
            Gametimer = new Timer();
            Gametimer.LoadContent(Content);
            Gametimer.setTimer(gameLenght);
            
            Gametimer.ourTimerCallBack += EndGame;
           
            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            //ourCritter.Input();
            foreach (Critter eachCritter in critterPool)
            {
                //loop contents
                eachCritter.Input();

            }

            button.Input();

            if (play == true)
            {
                timeUntilNextSpawn -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (timeUntilNextSpawn <= 0f)
                {
                    timeUntilNextSpawn = spawnDelay;
                    //spawn a critter


                    //spawn next critter in list
                    critterPool[currentCritterIndex].Spawn(Window);
                    ++currentCritterIndex;
                    //if we went pastr the end of the array wrap back to 0
                    if (currentCritterIndex >= critterPool.Length)
                    {
                        currentCritterIndex = 0;
                    }
                }

                Gametimer.Update(gameTime);
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            button.Draw(spriteBatch);

            foreach (Critter eachCritter in critterPool)
            {
                //loop contents
                eachCritter.Draw(spriteBatch);
            }
            ourScore.Draw(spriteBatch);
            Gametimer.Draw(spriteBatch);

            spriteBatch.End();
            

            base.Draw(gameTime);
        }

        void StartGame()
        {
            play = true;
            Gametimer.startTimer();
            ourScore.resetScore();
        }

        void EndGame()
        {
            play = false;

            //TODO show button
            button.Show();
            Gametimer.setTimer(gameLenght);


            //despawn all critters
            foreach(Critter eachCritter in critterPool)
            {
                eachCritter.Despawn();
            }

        }
    }
}
