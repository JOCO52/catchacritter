﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Catch_A_Critter
{
    class Button
    {
        //---------------------------
        //Data
        //---------------------------
        bool Visible = true;
        Texture2D image = null;
        Vector2 position = Vector2.Zero;
        SoundEffect click = null;

        //delegates
        //the delegate definition states what types of functions are allowed
      public delegate void OnClick();
        public OnClick ourButtonCallBack;


        //----------------------------
        //Behaviour
        //----------------------------

        public void LoadContent(ContentManager content)
        {
            image = content.Load<Texture2D>("graphics/button");
            click = content.Load<SoundEffect>("audio/buttonclick");
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (Visible == true)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
        }

        public void Input()
        {
            //get curretn status of mouse
            MouseState currentState = Mouse.GetState();

            //get the BoundingBox for button
            Rectangle Bounds = new Rectangle((int)position.X,
                (int)position.Y,
                image.Width,
                image.Height
                );

            //check if we have the left button held down over the bytton and if the button is visible
            if (currentState.LeftButton == ButtonState.Pressed
               && Bounds.Contains(currentState.X, currentState.Y)
               && Visible == true)
            {
                //we clicked the critter
                click.Play();

                Visible = false;
                
                //start the game
                if( ourButtonCallBack != null)
                ourButtonCallBack();
            }
        }

        //----------------------
        public void Show()
        {
            Visible = true;
        }
        //----------------------
    }
}
