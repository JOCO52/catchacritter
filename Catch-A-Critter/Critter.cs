﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Catch_A_Critter
{
    class Critter
    {
        //-------------------------
        //Type Definitions
        //-------------------------
        public enum Species
        // A special type of integer with only specific values allowed that have name
        {
            CHICKEN, // = 0
            CROCODILE, // = 1
            GORILLA, // = 2

            //--

            NUM // = 3

        }


        //----------------
        //data
        //----------------
        Texture2D image = null;
        Vector2 position = Vector2.Zero;
        bool alive = false;
        Score scoreObject = null;
        int scorevalue = 10;
        SoundEffect Click = null;
        Species species = Species.CHICKEN; // 0 = chicken, 1 = crocodile, 2 = tbd


        //-----------------------
        //Behaviour
        //------------------------

            public Critter(Score Newscore, Species newSpecies)
        {
            //constructor called when the object is cretaed, no return type, helps decide how the object will be set up, can have arguments, This One lets us have acess to the game score
            scoreObject = Newscore;
            species = newSpecies;


        }

        public void LoadContent(ContentManager content)
        {
            //image = content.Load<Texture2D>("graphics/chicken");

            switch(species)
            {
                case Species.CROCODILE:
                    image = content.Load<Texture2D>("graphics/crocodile");
                    scorevalue = 50;
                    break;

                case Species.CHICKEN:
                    image = content.Load<Texture2D>("graphics/chicken");
                    scorevalue = 10;
                    break;

                case Species.GORILLA:
                   image = content.Load<Texture2D>("graphics/gorilla");
                    scorevalue = 100;
                    break;

                default:
                    //this should never happen
                    break;
            }

            Click = content.Load<SoundEffect>("audio/buttonclick");
        }


        public void Draw(SpriteBatch spriteBatch)
        {
            if (alive == true)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
        }

        public void Spawn(GameWindow window)
        {
            //set critter to be visible
            alive = true;

            //determine bounds for random location
            int PositionYmin = 0;
            int PositionXmin = 0;
            int PositionYMax = window.ClientBounds.Height - image.Height;
            int PositionXMax = window.ClientBounds.Width - image.Width;

            //generate random loacation within bounds
            Random rand = new Random();
            position.X = rand.Next(PositionXmin, PositionXMax);
            position.Y = rand.Next(PositionYmin, PositionYMax);
        }
        //--------------------
        public void Despawn()
        {
            // set critter to be invisible
            alive = false;

        }

        //-------------
        public void Input()
        {
            //get curretn status of mouse
            MouseState currentState = Mouse.GetState();

            //get the BoundingBox for Critter
            Rectangle critterBounds = new Rectangle((int)position.X,
                (int)position.Y,
                image.Width,
                image.Height
                );

            //check if we have the left button held down over the critter and if the critter is visible
            if (currentState.LeftButton == ButtonState.Pressed
               && critterBounds.Contains(currentState.X, currentState.Y)
               && alive== true)
            {
                //we clicked the critter
                Click.Play();
                //despawn critter
                Despawn();

                //add to score(TODO)
                scoreObject.AddScore(scorevalue);
            }
        }


    }
}
