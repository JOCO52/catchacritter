﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Catch_A_Critter
{
    class Timer
    {
        //---------------------
        //Data
        //-----------------------

        float time = 60f;
        Vector2 position = new Vector2(10, 30);
        SpriteFont font = null;
        bool running = false;

        //delegates
        //the delegate definition states what types of functions are allowed
        public delegate void TimeUP();
        public TimeUP ourTimerCallBack;

        //-----------------
        //Behaviour
        //------------------

        public void LoadContent(ContentManager content)
        {
            font = content.Load<SpriteFont>("fonts/MainFont");
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //draw the time to screen using font provided
            int timeInt = (int)time;
            spriteBatch.DrawString(font, "Time: " + timeInt.ToString(), position, Color.White);

        }

        public void Update(GameTime gameTime)
        {
            if (running == true)
            {
                time -= ((float)gameTime.ElapsedGameTime.TotalSeconds);


                if (time <= 0)
                {
                    //stop
                    running = false;
                    time = 0;

                    if (ourTimerCallBack != null)
                    {
                        ourTimerCallBack();
                    }
                }
            }
        }

        public void startTimer()
        {
            running = true;
            
        }

        public void setTimer(float newTime)
        {
            time = newTime;
        }

        public void Reset()
        {
            time = 30f;
        }
    }
}
