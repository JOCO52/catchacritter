﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Catch_A_Critter
{
    class Score
    {
        //-----------------------
        //data
        //-----------------------

        int value = 0;
        Vector2 position = new Vector2(10, 10);
        SpriteFont font = null;

        //--------------------------
        //Behaviour
        //--------------------------
        public void LoadContent(ContentManager content)
        {
            font = content.Load<SpriteFont>("fonts/MainFont");
        }

        public void AddScore(int toadd)

        {
            //add the provided number to our curent score
            value += toadd;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //draw the text score to screen using font provided
            spriteBatch.DrawString(font, "Score: " + value.ToString(), position, Color.White);

        }

        public void resetScore()
        {
            value = 0;
        }

    }
}
